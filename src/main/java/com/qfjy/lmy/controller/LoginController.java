package com.qfjy.lmy.controller;

import com.qfjy.service.impl.LoginServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Controller
public class LoginController {

    @Resource
    private LoginServiceImpl loginService;

    @RequestMapping("/checkCode")
    @ResponseBody
    public String login(@RequestParam("phone") String phone,
                        @RequestParam("password") String password,
                        HttpServletRequest request){

        //先判断用户的ip地址是否被锁住
        String ipAddr = request.getRemoteAddr();
        boolean isLocked = loginService.checkIpLocked(ipAddr);
        if (isLocked){
            return "ip被锁，2分钟试";
        }

        //判断密码是否正确，
        String realPwd = loginService.getPwdByPhone(phone);
        if (Objects.equals(realPwd,password)){
            return "登录成功";
        }else{
            //密码错误则给当前IP的数量加一，到五则锁住ip
            loginService.ipAddrInr(ipAddr);
            return "密码错误";
        }
    }
}
