package com.qfjy.service.impl;

import com.qfjy.entity.po.UserPO;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class LoginServiceImpl {

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    public boolean checkIpLocked(String ipAddr){
        if(redisTemplate.hasKey(ipAddr)){
            return (boolean) redisTemplate.opsForHash().get(ipAddr,"lock");
        }
        return false;
    }

    public String getPwdByPhone(String phone){

        if (redisTemplate.opsForHash().hasKey("user",phone)){
            UserPO user1 = (UserPO) redisTemplate.opsForHash().get("user", phone);
            return user1.getName();
        }
        UserPO user = new UserPO();
        user.setId(phone);
        user.setName("123456");
        redisTemplate.opsForHash().put("user",phone,user);
        UserPO user1 = (UserPO) redisTemplate.opsForHash().get("user", phone);
        return user1.getName();
    }

    public void ipAddrInr(String ipAddr){

        if(redisTemplate.hasKey(ipAddr)){
            Integer count = (Integer) redisTemplate.opsForHash().get(ipAddr,"count");
            count++;
            if(count==5){
                redisTemplate.opsForHash().put(ipAddr,"lock",true);
                redisTemplate.expire(ipAddr,120, TimeUnit.SECONDS);
            }
            redisTemplate.opsForHash().put(ipAddr,"count",count);
        }else {
            redisTemplate.opsForHash().put(ipAddr,"count",1);
            redisTemplate.opsForHash().put(ipAddr,"lock",false);
        }
    }
}
